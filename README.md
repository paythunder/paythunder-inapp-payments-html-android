PayThunder API for Android
========================

[PayThunder](http://www.paythunder.com/) allows mobile payments & marketing as well as additional services combined with merchants applications, or working with the merchant POS

Integration instructions
------------------------

### Requirements

*   Android SDK version 15 or later.

### Setup


##### If you use gradle, then add the following dependency:

```
compile project(':paythunder')
```

##### If you use something other than gradle, then:

1. Edit AndroidManifest.xml. We're going to add a few additional items in here:

    ```xml
    <uses-sdk android:minSdkVersion="15" />
    ```

2. Also in your `<manifest>` element, make sure the following permissions and features are present:

    ```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    ```

3. Within the `<application>` element, add activity entries:

    ```xml
    <!-- Activities responsible for gathering payment info -->
    <activity android:name="com.paythunder.api.PayThunderActivity" />
    ```

##### Note: Before you build in release mode, make sure to adjust your proguard configuration by adding the following to `proguard.cnf`:

```
-keep class com.paythunder.**
-keepclassmembers class com.paythunder.** {
    *;
}
```

### Sample code  (See the PayThunderAPITest App for an example)

First, we'll assume that you're going to launch the request from a button,
and that you've set the button's `onClick` handler in the layout XML via `android:onClick="requestPayThunderPayment"`.
Then, add the method as:

```java
public void requestPayThunderPayment(View v) {
    Intent payThunderIntent = new Intent(this, PayThunderActivity.class);
    Bundle bundle = new Bundle();

    // customize these values to suit your needs.
    bundle.putString(Constants.REQUEST_CODIGO_COMERCIO, "ESP-B14000000");
    bundle.putString(Constants.REQUEST_CODIGO_TERMINAL, "001");
    bundle.putString(Constants.REQUEST_IMPORTE, "5");
    bundle.putString(Constants.REQUEST_DIVISA, "EUR");
    bundle.putString(Constants.REQUEST_DESCRIPCION, "Descripcion de prueba");
    bundle.putString(Constants.REQUEST_EMAIL, "test@email.com");
    bundle.putString(Constants.REQUEST_MOVIL, "666666666");
    bundle.putString(Constants.REQUEST_MOVIL_COUNTRY_CODE, "+34");
    bundle.putString(Constants.REQUEST_CODIGO_PETICION_COMERCIO, "11");
    bundle.putString(Constants.REQUEST_REQUEST_DATA, "Informacion de peticion original");
    bundle.putString(Constants.REQUEST_CLAVE, "01a3ad1e3d04207fc3bf10074edc6880f0b1ff6f");

    // PAYTHUNDER_REQUEST is arbitrary and is only used within this activity.
    payThunderIntent.putExtras(bundle);
    startActivityForResult(payThunderIntent, PAYTHUNDER_REQUEST);
}
```

Next, we'll override `onActivityResult()` to get the request result.

```java
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == PAYTHUNDER_REQUEST) {

        switch (resultCode) {
            case RESULT_OK:
                if (data != null) {
                    PaymentReply dataPaymentReply = new PaymentReply();
                    dataPaymentReply = (PaymentReply) data.getSerializableExtra(PayThunderActivity.EXTRA_PAYMENT_RESULT);

                    // do something with dataPaymentReply

                }
                break;
            case RESULT_FIRST_USER:
                if (data != null) {
                    String error = data.getStringExtra(PayThunderActivity.EXTRA_PAYMENT_RESULT);

                    // do something with the error String

                }
                break;
            case RESULT_CANCELED:

                // do something with canceled operation

                break;
            default:

                //error

                break;
        }
    } // else handle other activity results

```
